from AirBrayton import Intake
from AirBrayton import Compressor
from AirBrayton import CombustionChamber
from AirBrayton import Turbine
from AirBrayton import Nozzle
from AirBrayton import CalculateCp

# inputs

flight_speed            = 0.01 * 324  # m/s

altitude                = 0  # m

# inlet conditions

in_temp                 = 288.08 -0.00649 *altitude

in_press                = (in_temp /288.08 )**5.256 *101325

in_density              = in_press /(286.9 *in_temp)

flight_Mach             = flight_speed /(324 *(in_temp /288.08 )**0.5)

# efficiencies

intake_eff              = 1.0

comp_eff                = 0.82

turbine_eff             = 0.85

combustor_eff           = 0.95

nozzle_eff              = 0.92 *(0.9974 - 0.4769 * flight_Mach + 0.448 * flight_Mach ** 2)

# flow rates

massflowrate            = 0.668  # kg/s

fuelflowrate            = 1040  # g/min

after_fuelflowrate      = 0  # g/min

# typical value

comp_ratio              = 3.8

# critical pressure

p_c                     = 1 / ((1 + 0.2) ** (1.4 / 0.4))

exp_ratio               = 1.5

intake                  = Intake(intake_eff, in_temp, in_press, massflowrate, flight_speed, altitude)

intake.compute(in_density)

if flight_speed > 320:

    compressor      = Compressor(comp_eff, intake.out_temp, intake.out_press, massflowrate, comp_ratio)

else:

    compressor      = Compressor(comp_eff, in_temp * (1 + 0.2 * flight_Mach ** 2), in_press * (1 + 0.2 * flight_Mach ** 2) ** 3.5, massflowrate, comp_ratio)

compressor.compute()

combustion_chamber  = CombustionChamber(combustor_eff, compressor.out_temp, compressor.out_press, compressor.massflowrate, fuelflowrate)

combustion_chamber.compute()

turbine             = Turbine(turbine_eff, combustion_chamber.out_temp, combustion_chamber.out_press, combustion_chamber.massflowrate, exp_ratio)

while True:

    turbine.compute()

    if abs(-compressor.power / turbine.power - 1.) < 0.002:

        break

    else:

        if -compressor.power / turbine.power < 1.:

            turbine.exp_ratio -= 0.0001

        else:

            turbine.exp_ratio += 0.0001


# after_burner            = CombustionChamber(combustor_eff, turbine.out_temp, turbine.out_press, turbine.massflowrate, after_fuelflowrate)
#
# after_burner.compute()

nozzle                  = Nozzle(nozzle_eff, combustion_chamber.out_temp, combustion_chamber.out_press, combustion_chamber.massflowrate)

nozzle.compute(flight_speed, in_press)

print('INPUTS')

print('')

print('flight_speed= ', flight_speed, 'm/s  Mach#= ', flight_speed / (324 * (in_temp / 288.08) ** 0.5))

print('altitude= ', altitude, 'm')

print('mdot_air= ', compressor.massflowrate, 'kg/s')

print('comp_ratio= ', compressor.comp_ratio)

print('')

print('OUTPUTS')

if flight_speed > 320:

    print('')

    print('Intake')

    print('')

    print('intake diameter= ', (intake.area * 4 / 3.14) ** 0.5, 'm')

    print('')

print('Compressor')

print('_______________________________________________________')

print('comp_power= ', -compressor.power, 'kW')

print('')

print('Combustion chamber')

print('_______________________________________________________')

print('fc= ', (fuelflowrate), 'g/min')

print('heat_in= ', combustion_chamber.power, 'kW')

print('flame_temp= ', combustion_chamber.out_temp - 273, 'deg C')

print('turbine_power= ', turbine.power, 'kW')

print('')

print('Turbine')

print('_______________________________________________________')

print('turbine_inlet_temperature= ', combustion_chamber.out_temp, 'deg K')

print('turbine in press= ', turbine.in_press / in_press)

print('turbine out temp= ', turbine.out_temp - 273, 'deg C')

print('turbine out press= ', turbine.out_press / in_press, 'Pa', '     critical press= ', 1 / p_c)

print('turbine exp ratio= ', turbine.exp_ratio)

print('comp/turb power ratio= ', -compressor.power / turbine.power)
#
# print('')
#
# print('Afterburner')
#
# print('_______________________________________________________')
#
# print('fc= ', (after_fuelflowrate), 'g/min')
#
# print('afterburner out temp= ', after_burner.out_temp - 273, 'deg C', '     critical temp= 1700 deg C')

print('')

print('Exhaust')

print('_______________________________________________________')

print('exhaust temp= ', nozzle.out_temp - 273, 'C')

print('exhaust gas speed= ', nozzle.exit_vel)

print('nozzle diameter= ', (nozzle.area * 4 / 3.14) ** 0.5, 'm')

print('')

print('General')

print('_______________________________________________________')

print('sfc= ', (fuelflowrate + after_fuelflowrate) / 1000. * 60 / (nozzle.trust), 'kg/N/hr')

print('trust= ', nozzle.trust, 'N')