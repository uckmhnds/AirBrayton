
class Intake(object):

    def __init__ (self ,efficiency ,in_temp ,in_press ,massflowrate ,speed ,altitude):

        self.efficiency =efficiency

        self.in_temp =in_temp

        self.in_press =in_press

        self.massflowrate =massflowrate

        self.speed =speed

        self.altitude =altitude

    def compute(self ,in_density):

        self.sound =324 *(self.in_temp /288.08 )**0.5

        self.in_mach =self.speed /self.sound

        self.out_temp =(2. *1.4 *self.in_mach** 2 -0.4 ) *(0.4 *self.in_mach** 2 +2 ) / (2.4** 2 *self.in_mach**2 ) *self.in_temp

        self.out_press =(2. *1.4 *self.in_mach** 2 -0.4 ) /2.4 *self.in_press

        self.out_density =in_density *(2.4 *self.in_mach**2 ) /(0.4 *self.in_mach** 2 +2)

        self.out_mach =(0.4 *self.in_mach** 2 +2 ) /( 2 *1.4 *self.in_mach** 2 -0.4)

        self.area =self.massflowrate /(self.out_density *self.out_mach *self.sound)

class Compressor(object):

    def __init__ (self ,efficiency ,in_temp ,in_press ,massflowrate ,comp_ratio):

        self.efficiency =efficiency

        self.in_temp =in_temp

        self.in_press =in_press

        self.massflowrate =massflowrate

        self.comp_ratio =comp_ratio

    def compute(self):

        self.out_temp =self.in_temp *(self.comp_ratio )**(0.4 /1.4)

        self.out_press =self.in_press *self.comp_ratio

        self.power =self.massflowrate *CalculateCp(0.5 *(self.in_temp +self.out_temp) ) * (self.in_temp -self.out_temp ) /self.efficiency

class CombustionChamber(object):

    def __init__ (self ,efficiency ,in_temp ,in_press ,massflowrate ,fuelflowrate):

        self.efficiency =efficiency

        self.in_temp =in_temp

        self.in_press =in_press

        self.out_temp =in_temp

        self.massflowrate =massflowrate

        self.fuelflowrate =fuelflowrate /60. /1000.

    def compute(self):

        self.power =self.fuelflowrate *43.15 *1000  # kW

        for i in range(0 ,5):

            self.out_temp =self.power *self.efficiency /  (self.massflowrate *CalculateCp(0.5 *(self.in_temp +self.out_temp)) ) +self.in_temp

        self.out_press =0.9 *self.in_press

class Turbine(object):

    def __init__ (self ,efficiency ,in_temp ,in_press ,massflowrate ,exp_ratio):

        self.efficiency =efficiency

        self.in_temp =in_temp

        self.in_press =in_press

        self.massflowrate =massflowrate

        self.exp_ratio =exp_ratio

    def compute(self):

        self.out_temp =self.in_temp *(1. /self.exp_ratio )**(0.4 /1.4)

        self.power =self.massflowrate *CalculateCp(0.5 *(self.in_temp +self.out_temp) ) * \
                    (self.in_temp -self.out_temp ) *self.efficiency

        self.out_press =self.in_press /self.exp_ratio

class Nozzle(object):

    def __init__ (self ,efficiency ,in_temp ,in_press ,massflowrate):

        self.efficiency =efficiency

        self.in_temp =in_temp

        self.in_press =in_press

        self.massflowrate =massflowrate



    def compute(self ,flight_speed ,in_press):

        self.out_press =self.in_press /((2.33 /2 )**(1.33 /0.33))

        self.out_temp =self.in_temp /((2.33 /2))

        self.exit_vel =324 *(self.in_temp /288.08 )**0.5

        self.area =self.massflowrate *(324 *(self.in_temp /288.08 )**0.5 *(2.33 /2 )**(2.33 /0.66) ) / (self.in_press *1.4)

        self.trust =self.massflowrate *(self.exit_vel *self.efficiency -flight_speed)

def CalculateCp(temp):

    cp =(1.9327e-10 *temp** 4 -8e-7 *temp** 3 +1.1407e-3 *temp** 2 -4.489e-1 *temp +1.0575e3 ) /1000.

    return cp  # kj/kgK

